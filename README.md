# Jhecht's TSlint Config

Greetings, this is simply the TSLint configuration that I use with a fair bit of frequency. While this is the general way I do things, I will likely update this in the near future as I remember more obscure settings I've had to tweak in other projects.f

## Usage

To use in your project, simply run 
```
yarn add --dev tslint-config-jhecht
```
Or, if using NPM:
```
$ npm i -D tslint-config-jhecht
```

## Notices:
These are my personal preferences -- you're welcome to use them, and you're also welcome to not use them. If you like _most_ of my settings, but disagree with others, then you can simply extend by configuration by following the steps above, and then in your project root creating a `tslint.json` and adding in the following lines:

```
{
    "extends": "tslint-config-jhecht",
    "rules": {
        // the rules I have set whose values you would like to update
    }
}
```