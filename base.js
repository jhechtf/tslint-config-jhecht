module.exports = {
    "extends": "tslint:recommended",
    "rules": {
        "whitespace": {
            "options": ["check-decl", "check-type", "check-type-operator"]
        },
        "class-name": false,
        "interface-name": false,
        "quotemark": {
            "options": ["single", "avoid-escape"]
        },
        "max-line-length": false,
        "no-console": false,
        "max-file-line-count": false,
        "object-literal-sort-keys": false,
        "trailing-comma": false,
        "variable-name": [true,"allow-leading-underscore"],
        "curly": {
            "options": ["ignore-same-line"]
        }
    }
};